import { Component } from '@angular/core';

@Component({
  selector: 'test-nx-ci-commit-before-sha-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'app-c';
}
